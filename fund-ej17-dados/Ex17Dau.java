package ExAl;

import ExercicisInicialS22.*;
import static ExercicisInicialsS2.Exemple5_ArrayChar.solucio2;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Simula 10 vegades la tirada d’un dau (generar aleatòriament un número del 1
 * al numero de cares que tingui el dau) e que imprimeixi els resultats per 
 * pantalla en una sola línea separada por comas.
 * 4,2,2,6,1,3,1,...
 * @author alumne
 */
public class Ex17Dau {

    public static void main(String[] args) {
        int numAleatorio;
        final int DADO_6CARAS = 6;
        final int NUM_TIRADES = 10;
        // un numero del 1 al 6  
        System.out.println("Tirar 10 vegades un dau:");
        
        String resultats = "";
        for (int i = 0; i < NUM_TIRADES; i++) {
            resultats += (int) (Math.random() * DADO_6CARAS) + 1;
            if(i<NUM_TIRADES-1)
                resultats += ",";
        }
        resultats += ".";
        System.out.println(resultats);
    }
}
