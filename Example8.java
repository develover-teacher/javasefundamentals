
package example8;

/**
 * Donada una frase, separar-ho en paraules usant el metode split.
 */
public class Example8 {
    public static void main(String[] args) {
    String s = "ola k ase, ase java de nuevo o k ase";
        String[] words = s.split("\\W+");//expresión regular caract. Sólo ASCII y una letra como mínimo

        for ( String ss : words) {
            System.out.println(ss);
        }
    }
}
