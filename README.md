# JavaSEFundamentals

Ejercicios de Java SE para aprender conceptos del lenguaje fundamentales.

## Ej17. Dados
Simula 10 vegades la tirada d’un dau (generar aleatòriament un número del 1 al numero de cares que tingui el dau) e que imprimeixi els resultats per 
pantalla en una sola línea separada por comas. 4,2,2,6,1,3,1,...


## Ej8. Separar frase en palabras
Donada una frase, separar-ho en paraules usant el metode split.
